from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse #temporaire
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.http import Http404
from riddle.models import Riddle, Exercice, Correction
from django.contrib.auth.models import User
from django import forms
from django.utils import timezone


def index(request):
    if not request.user.is_authenticated():
        return redirect('/admin/login/?next=%s'%request.path)
    lastest_riddle_list = Riddle.objects.order_by('-pub_date')
    enriched_list = []
    for item in lastest_riddle_list:
        nb_response = Exercice.objects.filter(riddle=item).filter(user=request.user)
        last_response = Exercice.objects.filter(riddle=item).filter(user=request.user).order_by('-pub_date')[:1]
        note = 0
        nb_note = 0
        for item_note in nb_response:
            note += item_note.note
            nb_note += 1
        str_note = ''
        if nb_note != 0:
            note = note / nb_note
            str_note = str(note) + '/20'
#TODO: mettre une image none quand il n'y a pas de note du tout
        if note == 0:
            grade = 'riddle/bronze.png'
        elif note > 0 and note <= 5:
            grade = 'riddle/silver.png'
        elif note > 5 and note <= 10:
            grade = 'riddle/gold.png'
        elif note > 10 and note <= 12:
            grade = 'riddle/diamond.png'
        elif note > 12 and note <= 15:
            grade = 'riddle/platinium.png'
        elif note > 15 and note <= 18:
            grade = 'riddle/master.png'
        else:
            grade = 'riddle/grand-master.png'
        str_id = ''
        if len(last_response) > 0:
            str_id = str(last_response[0].id)
        enriched_list.append((item, str(len(nb_response)), str_id, str_note, grade))
    context={'lastest_riddle_list': enriched_list}
    return render(request, 'riddle/index.html', context)

class RenduForm(forms.Form):
    rendu = forms.CharField(label="", widget=forms.Textarea(attrs={'cols': 80, 'rows': 38}))

@login_required(login_url='/admin/login/')
def riddle(request, riddle_id):
    theriddle = get_object_or_404(Riddle, pk=riddle_id)
    if request.method == 'POST':
        form = RenduForm(request.POST)
        nb_response = Exercice.objects.filter(riddle=theriddle).filter(note=20).filter(user=request.user)
        if len(nb_response) > 0:
            return redirect('/')
        if form.is_valid():
            exo = Exercice()
            exo.content = form.cleaned_data['rendu']
            exo.user = request.user
            exo.riddle = theriddle
            exo.pub_date = timezone.now()
            exo.note = 0
            if exo.riddle.needCompile:
                exo.note=0
            else:
                last_correction = Correction.objects.filter(riddle=exo.riddle).filter(not_the_best_solution=False)[:1]
                if len(last_correction) != 0:
                    if last_correction[0].content == exo.content:
                        exo.note = 20
            exo.save()
            return redirect('/')
    else:
        form = RenduForm()
    return render(request, 'riddle/detail.html', {'riddle':theriddle, 'form':form})

def hint(request, hint_id):
    return HttpResponse("Hint id=%s" % hint_id)

def correction(request, correction_id):
    return HttpResponse("Correction id=%s" % correction_id)

def exercices(request):
    return HttpResponse("List des rendu")

def exercice(request, exercice_id):
    exo = get_object_or_404(Exercice, pk=exercice_id)
    if exo.user == request.user : 
        return HttpResponse(exo.content)
    return redirect('/')

def classement(request):
    users = User.objects.order_by('username')
    enrichedList = []
    for user in users:
        rendus = Exercice.objects.filter(user=user)
        test =[]
        for rendu in rendus:
            riddles = Riddle.objects.filter(pk=rendu.riddle.id).order_by('-pub_date')
            if len(riddles) > 0:
                test.append((rendu, riddles[0].name))
            else:
                test.append((rendu, ''))
        enrichedList.append((user, test))
    return render(request, 'riddle/classement.html', {'users':enrichedList})

