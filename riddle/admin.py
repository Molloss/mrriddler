from django.contrib import admin
from riddle.models import Riddle
from riddle.models import Correction
from riddle.models import Exercice
from riddle.models import Hint

class CorrectionInLine(admin.StackedInline):
    model = Correction
    extra = 0

class HintInLine(admin.StackedInline):
    model = Hint
    extra = 1

class ExerciceTab(admin.TabularInline):
    model = Exercice
    extra = 1

class RiddleAdmin(admin.ModelAdmin):
    list_filter = ['pub_date']
    search_fields = ['question']
    inlines = [CorrectionInLine, HintInLine]

admin.site.register(Riddle, RiddleAdmin)
admin.site.register(Correction)
admin.site.register(Exercice)
admin.site.register(Hint)
