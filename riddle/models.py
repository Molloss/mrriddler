from django.db import models
import datetime
from django.utils import timezone
from django.conf import settings

class Riddle(models.Model):
    def __str__(self):
        return self.name
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'
    pub_date = models.DateTimeField('date published')
    name = models.CharField(max_length=200)
    content = models.TextField()
    admin = models.ForeignKey(settings.AUTH_USER_MODEL)
    needCompile = models.BooleanField('Need Compil', default=False)

class Correction(models.Model):
    def __str__(self):
        return '[' +  self.riddle.__str__() + ']' + self.admin.__str__() + ' - ' + self.pub_date.__str__()
    riddle = models.ForeignKey(Riddle)
    pub_date = models.DateTimeField('date published')
    not_the_best_solution = models.BooleanField('Not the best solution', default=False)
    admin = models.ForeignKey(settings.AUTH_USER_MODEL)
    content = models.TextField()

class Hint(models.Model):
    def __str__(self):
        return "Hint for " + self.riddle.__str__()
    riddle = models.ForeignKey(Riddle)
    order = models.IntegerField()
    pub_date = models.DateTimeField('date published')
    content = models.TextField()

class Exercice(models.Model):
    def __str__(self):
        return '[' + self.riddle.__str__() + '] ('+str(self.note)+') ' + self.user.__str__() + ' - ' + self.pub_date.__str__()
    riddle = models.ForeignKey(Riddle)
    pub_date = models.DateTimeField('date published')
    content = models.TextField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    note = models.IntegerField(default=0)

