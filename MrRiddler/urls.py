from django.conf.urls import include, url
from django.contrib import admin

from riddle import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'MrRiddler.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', views.index, name='index'),
    url(r'^(?P<riddle_id>\d+)/$', views.riddle, name='riddle'),
    url(r'^hint/(?P<hint_id>\d+)$', views.hint, name='hint'),
    url(r'^correction/(?P<correction_id>\d+)$', views.correction, name='correction'),
    url(r'^rendu$', views.exercices, name='exercices'),
    url(r'^classement$', views.classement, name='classement'),
    url(r'^rendu/(?P<exercice_id>\d+)$', views.exercice, name='exercice'),
]
